/*
 * Copyright (C) 2021 ChromaWay AB. See LICENSE for license information.
 */

package net.postchain.rell.lang.expr.atexpr

class AtExprCommonColEntityTest: AtExprCommonColBaseTest() {
    override fun impKind() = AtExprTestKind_Col_Entity()
}
