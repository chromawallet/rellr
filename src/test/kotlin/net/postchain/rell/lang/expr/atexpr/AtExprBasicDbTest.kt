/*
 * Copyright (C) 2021 ChromaWay AB. See LICENSE for license information.
 */

package net.postchain.rell.lang.expr.atexpr

class AtExprBasicDbTest: AtExprBasicBaseTest() {
    override fun impKind() = AtExprTestKind_Db()
}
