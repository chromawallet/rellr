/*
 * Copyright (C) 2020 ChromaWay AB. See LICENSE for license information.
 */

package net.postchain.rell.misc

import net.postchain.gtv.GtvDecoder
import net.postchain.rell.compiler.base.utils.C_SourceDir
import net.postchain.rell.module.RellVersions
import net.postchain.rell.test.*
import net.postchain.rell.tools.runcfg.RellRunConfigGenerator
import net.postchain.rell.tools.runcfg.RellRunConfigParams
import net.postchain.rell.utils.*
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class RunConfigGenTest {
    @Test fun testNodeConfig() {
        val configFiles = mapOf(
                "my-config.properties" to """
                    include=private.properties
                    foo=123
                    node.0.pubkey=0350fe40766bc0ce8d08b3f5b810e49a8352fdd458606bd5fafe5acdcdc8ff3f57
                """.trimIndent(),
                "private.properties" to "bar=456\n",
                "other.properties" to "other=789"
        )

        val files = generate(mapOf(), configFiles, """
            <run>
                <nodes>
                    <config src="my-config.properties"/>
                </nodes>
                <chains>
                    <chain name="user" iid="33">
                        <config height="0">
                        </config>
                    </chain>
                </chains>
            </run>
        """)

        chkFile(files, "node-config.properties", """
            include=private.properties
            foo=123
            node.0.pubkey=0350fe40766bc0ce8d08b3f5b810e49a8352fdd458606bd5fafe5acdcdc8ff3f57
        """)

        chkFile(files, "private.properties", "bar=456")

        chkFile(files, "blockchains/33/brid.txt", "A5D2F114B70602A5145FB705EC4FED482F47B2AAB9780DBD17564DF1E4150F99")

        chkFile(files, "blockchains/33/0.xml", """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
            <dict>
                <entry key="signers">
                    <array>
                        <bytea>0350FE40766BC0CE8D08B3F5B810E49A8352FDD458606BD5FAFE5ACDCDC8FF3F57</bytea>
                    </array>
                </entry>
            </dict>
        """)

        chkFileBin(files, "blockchains/33/0.gtv", """{"signers":["0350FE40766BC0CE8D08B3F5B810E49A8352FDD458606BD5FAFE5ACDCDC8FF3F57"]}""")

        assertEquals(setOf<String>(), files.keys)
    }

    @Test fun testAddSignersFalse() {
        val configFiles = mapOf(
                "my-config.properties" to "node.0.pubkey=0350fe40766bc0ce8d08b3f5b810e49a8352fdd458606bd5fafe5acdcdc8ff3f57"
        )

        val files = generate(mapOf(), configFiles, """
            <run>
                <nodes>
                    <config src="my-config.properties" add-signers="false"/>
                </nodes>
                <chains>
                    <chain name="user" iid="33">
                        <config height="0">
                        </config>
                    </chain>
                </chains>
            </run>
        """)

        chkFile(files, "node-config.properties", "node.0.pubkey=0350fe40766bc0ce8d08b3f5b810e49a8352fdd458606bd5fafe5acdcdc8ff3f57")
        chkFile(files, "blockchains/33/brid.txt", "F9161BC0ABE91C6ACAB68B4A0B18CCD5763AE5B5798BC522209DDCF6D0302630")
        chkFileBin(files, "blockchains/33/0.gtv", """{"signers":[]}""")

        chkFile(files, "blockchains/33/0.xml", """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
            <dict>
                <entry key="signers">
                    <array/>
                </entry>
            </dict>
        """)

        assertEquals(setOf<String>(), files.keys)
    }

    @Test fun testAddSignersDefault() {
        val configFiles = mapOf(
                "my-config.properties" to "node.0.pubkey=0350fe40766bc0ce8d08b3f5b810e49a8352fdd458606bd5fafe5acdcdc8ff3f57"
        )

        val files = generate(mapOf(), configFiles, """
            <run>
                <nodes>
                    <config src="my-config.properties"/>
                </nodes>
                <chains>
                    <chain name="user" iid="33">
                        <config height="0">
                        </config>
                    </chain>
                </chains>
            </run>
        """)

        chkFile(files, "node-config.properties", "node.0.pubkey=0350fe40766bc0ce8d08b3f5b810e49a8352fdd458606bd5fafe5acdcdc8ff3f57")
        chkFile(files, "blockchains/33/brid.txt", "A5D2F114B70602A5145FB705EC4FED482F47B2AAB9780DBD17564DF1E4150F99")
        chkFileBin(files, "blockchains/33/0.gtv", """{"signers":["0350FE40766BC0CE8D08B3F5B810E49A8352FDD458606BD5FAFE5ACDCDC8FF3F57"]}""")

        chkFile(files, "blockchains/33/0.xml", """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
            <dict>
                <entry key="signers">
                    <array>
                        <bytea>0350FE40766BC0CE8D08B3F5B810E49A8352FDD458606BD5FAFE5ACDCDC8FF3F57</bytea>
                    </array>
                </entry>
            </dict>
        """)

        assertEquals(setOf<String>(), files.keys)
    }

    @Test fun testAddSignersExisting() {
        val configFiles = mapOf(
                "my-config.properties" to "node.0.pubkey=0350fe40766bc0ce8d08b3f5b810e49a8352fdd458606bd5fafe5acdcdc8ff3f57"
        )

        val files = generate(mapOf(), configFiles, """
            <run>
                <nodes>
                    <config src="my-config.properties"/>
                </nodes>
                <chains>
                    <chain name="user" iid="33">
                        <config height="0">
                            <gtv path="signers">
                                <array>
                                    <bytea>987654321098765432109876543210987654321098765432109876543210ABCDEF</bytea>
                                </array>
                            </gtv>
                        </config>
                    </chain>
                </chains>
            </run>
        """)

        chkFile(files, "node-config.properties", "node.0.pubkey=0350fe40766bc0ce8d08b3f5b810e49a8352fdd458606bd5fafe5acdcdc8ff3f57")
        chkFile(files, "blockchains/33/brid.txt", "A5D2F114B70602A5145FB705EC4FED482F47B2AAB9780DBD17564DF1E4150F99")
        chkFileBin(files, "blockchains/33/0.gtv", """{"signers":["0350FE40766BC0CE8D08B3F5B810E49A8352FDD458606BD5FAFE5ACDCDC8FF3F57"]}""")

        chkFile(files, "blockchains/33/0.xml", """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
            <dict>
                <entry key="signers">
                    <array>
                        <bytea>0350FE40766BC0CE8D08B3F5B810E49A8352FDD458606BD5FAFE5ACDCDC8FF3F57</bytea>
                    </array>
                </entry>
            </dict>
        """)

        assertEquals(setOf<String>(), files.keys)
    }

    @Test fun testModule() {
        val sourceFiles = mapOf(
                "app.rell" to "module; import sub;\nfunction main(){}",
                "sub.rell" to "module; function sub(){}",
                "other.rell" to "module; function other(){}"
        )

        val files = generate(sourceFiles, mapOf(), """
            <run>
                <nodes>
                    <config>x=123</config>
                </nodes>
                <chains>
                    <chain name="user" iid="33">
                        <config height="0">
                            <app module="app" add-defaults="false"/>
                        </config>
                    </chain>
                </chains>
            </run>
        """)

        chkFile(files, "node-config.properties", "x=123")

        chkFile(files, "blockchains/33/brid.txt", "F86A2BD7F585D6F32AC27DFA4A8B1A16FC9604E1988EAED8C0B4C0D5619173E9")

        chkFile(files, "blockchains/33/0.xml", """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
            <dict>
                <entry key="gtx">
                    <dict>
                        <entry key="rell">
                            <dict>
                                <entry key="modules">
                                    <array>
                                        <string>app</string>
                                    </array>
                                </entry>
                                <entry key="sources">
                                    <dict>
                                        <entry key="app.rell">
                                            <string>module; import sub;
            function main(){}</string>
                                        </entry>
                                        <entry key="sub.rell">
                                            <string>module; function sub(){}</string>
                                        </entry>
                                    </dict>
                                </entry>
                                <entry key="version">
                                    <string>${RellTestUtils.RELL_VER}</string>
                                </entry>
                            </dict>
                        </entry>
                    </dict>
                </entry>
                <entry key="signers">
                    <array/>
                </entry>
            </dict>
        """)

        chkFileBin(files, "blockchains/33/0.gtv",
                """{
                    "gtx":{
                        "rell":{
                            "modules":["app"],
                            "sources":{
                                "app.rell":"module; import sub;\nfunction main(){}","sub.rell":"module; function sub(){}"
                            },
                            "version":"${RellTestUtils.RELL_VER}"
                        }
                    },
                    "signers":[]
                }""".unwrap()
        )

        assertEquals(setOf<String>(), files.keys)
    }

    @Test(expected = TestRellCliEnvExitException::class)
    fun testModuleNotFound() {
        generate(mapOf(), mapOf(), """
            <run>
                <nodes>
                    <config>x=123</config>
                </nodes>
                <chains>
                    <chain name="user" iid="33">
                        <config height="0">
                            <app module="app" />
                        </config>
                    </chain>
                </chains>
            </run>
        """)
    }

    @Test(expected = TestRellCliEnvExitException::class)
    fun testCompilationError() {
        val sourceFiles = mapOf(
                "app.rell" to "module; struct foo { x: unknown; }"
        )

        generate(sourceFiles, mapOf(), """
            <run>
                <nodes>
                    <config>x=123</config>
                </nodes>
                <chains>
                    <chain name="user" iid="33">
                        <config height="0">
                            <app module="app" add-defaults="false"/>
                        </config>
                    </chain>
                </chains>
            </run>
        """)
    }

    @Test(expected = TestRellCliEnvExitException::class)
    fun testTestModuleAsMainModule() {
        val sourceFiles = mapOf(
                "app.rell" to "@test module; struct foo { x: integer; }"
        )

        generate(sourceFiles, mapOf(), """
            <run>
                <nodes>
                    <config>x=123</config>
                </nodes>
                <chains>
                    <chain name="user" iid="33">
                        <config height="0">
                            <app module="app" />
                        </config>
                    </chain>
                </chains>
            </run>
        """)
    }

    @Test fun testDependenciesChainName() {
        chkDependencies(""" chain="user" """)
    }

    @Test fun testDependenciesBrid() {
        chkDependencies(""" brid="A5D2F114B70602A5145FB705EC4FED482F47B2AAB9780DBD17564DF1E4150F99" """)
    }

    private fun chkDependencies(depStr: String) {
        val files = generate(mapOf(), mapOf(), """
            <run>
                <nodes>
                    <config>
                        node.0.pubkey=0350fe40766bc0ce8d08b3f5b810e49a8352fdd458606bd5fafe5acdcdc8ff3f57
                    </config>
                </nodes>
                <chains>
                    <chain name="user" iid="33">
                        <config height="0"/>
                    </chain>
                    <chain name="city" iid="55">
                        <config height="0">
                            <dependencies>
                                <dependency name="user_dep" $depStr />
                            </dependencies>
                        </config>
                    </chain>
                </chains>
            </run>
        """)

        chkFile(files, "node-config.properties", "node.0.pubkey=0350fe40766bc0ce8d08b3f5b810e49a8352fdd458606bd5fafe5acdcdc8ff3f57")
        chkFile(files, "blockchains/33/brid.txt", "A5D2F114B70602A5145FB705EC4FED482F47B2AAB9780DBD17564DF1E4150F99")
        chkFileBin(files, "blockchains/33/0.gtv", """{"signers":["0350FE40766BC0CE8D08B3F5B810E49A8352FDD458606BD5FAFE5ACDCDC8FF3F57"]}""")
        chkFile(files, "blockchains/55/brid.txt", "D2DE94C67C3FAEDFB14D241FA902D3D4BA285F4C2813A6750F59CF502B5ADDEC")

        chkFileBin(files, "blockchains/55/0.gtv", """{
            "dependencies":[["user_dep","A5D2F114B70602A5145FB705EC4FED482F47B2AAB9780DBD17564DF1E4150F99"]],
            "signers":["0350FE40766BC0CE8D08B3F5B810E49A8352FDD458606BD5FAFE5ACDCDC8FF3F57"]}
        """.unwrap())

        chkFile(files, "blockchains/33/0.xml", """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
            <dict>
                <entry key="signers">
                    <array>
                        <bytea>0350FE40766BC0CE8D08B3F5B810E49A8352FDD458606BD5FAFE5ACDCDC8FF3F57</bytea>
                    </array>
                </entry>
            </dict>
        """)

        chkFile(files, "blockchains/55/0.xml", """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
            <dict>
                <entry key="dependencies">
                    <array>
                        <array>
                            <string>user_dep</string>
                            <bytea>A5D2F114B70602A5145FB705EC4FED482F47B2AAB9780DBD17564DF1E4150F99</bytea>
                        </array>
                    </array>
                </entry>
                <entry key="signers">
                    <array>
                        <bytea>0350FE40766BC0CE8D08B3F5B810E49A8352FDD458606BD5FAFE5ACDCDC8FF3F57</bytea>
                    </array>
                </entry>
            </dict>
        """)

        assertEquals(setOf<String>(), files.keys)
    }

    @Test(expected = RellCliErr::class)
    fun testDependencyWithGreaterIid() {
        generate(mapOf(), mapOf(), """
            <run>
                <nodes>
                    <config>
                        node.0.pubkey=0350fe40766bc0ce8d08b3f5b810e49a8352fdd458606bd5fafe5acdcdc8ff3f57
                    </config>
                </nodes>
                <chains>
                    <chain name="user" iid="55">
                        <config height="0"/>
                    </chain>
                    <chain name="city" iid="33">
                        <config height="0">
                            <dependencies>
                                <dependency name="user_dep" chain="user" />
                            </dependencies>
                        </config>
                    </chain>
                </chains>
            </run>
        """)
    }

    @Test fun testGeneratedBrid() {
        val files = generate(mapOf(), mapOf(), """
            <run>
                <nodes>
                    <config>
                        node.0.pubkey=0350fe40766bc0ce8d08b3f5b810e49a8352fdd458606bd5fafe5acdcdc8ff3f57
                    </config>
                </nodes>
                <chains>
                    <chain name="user" iid="33">
                        <config height="0"/>
                    </chain>
                    <chain name="city" iid="55">
                        <config height="0">
                            <dependencies>
                                <dependency name="user_dep" chain="user" />
                            </dependencies>
                        </config>
                    </chain>
                </chains>
            </run>
        """)

        chkFile(files, "node-config.properties", "node.0.pubkey=0350fe40766bc0ce8d08b3f5b810e49a8352fdd458606bd5fafe5acdcdc8ff3f57")
        chkFile(files, "blockchains/33/brid.txt", "A5D2F114B70602A5145FB705EC4FED482F47B2AAB9780DBD17564DF1E4150F99")
        chkFileBin(files, "blockchains/33/0.gtv", """{"signers":["0350FE40766BC0CE8D08B3F5B810E49A8352FDD458606BD5FAFE5ACDCDC8FF3F57"]}""")
        chkFile(files, "blockchains/55/brid.txt", "D2DE94C67C3FAEDFB14D241FA902D3D4BA285F4C2813A6750F59CF502B5ADDEC")

        chkFileBin(files, "blockchains/55/0.gtv", """{
            "dependencies":[["user_dep","A5D2F114B70602A5145FB705EC4FED482F47B2AAB9780DBD17564DF1E4150F99"]],
            "signers":["0350FE40766BC0CE8D08B3F5B810E49A8352FDD458606BD5FAFE5ACDCDC8FF3F57"]}
        """.unwrap())
    }

    @Test fun testTestTestConfig() {
        val sourceFiles = mapOf("app.rell" to "module;")

        val files = generate(sourceFiles, mapOf(), """
            <run>
                <nodes>
                    <config>x=123</config>
                    <test-config>y=456</test-config>
                </nodes>
                <chains>
                    <chain name="user" iid="33">
                        <config height="0"/>
                    </chain>
                </chains>
            </run>
        """)

        chkFile(files, "node-config.properties", "x=123")
        chkFile(files, "blockchains/33/brid.txt", "F9161BC0ABE91C6ACAB68B4A0B18CCD5763AE5B5798BC522209DDCF6D0302630")
        chkFileBin(files, "blockchains/33/0.gtv", """{"signers":[]}""")
    }

    @Test fun testTestTestModules() {
        val sourceFiles = mapOf("app.rell" to "module;")

        // Must not fail even when test modules don't really exist.
        val files = generate(sourceFiles, mapOf(), """
            <run>
                <nodes>
                    <config>x=123</config>
                </nodes>
                <chains>
                    <chain name="user" iid="33">
                        <config height="0"/>
                        <test module="tests.chain_test" />
                    </chain>
                    <test module="tests.common_test" />
                </chains>
            </run>
        """)

        chkFile(files, "node-config.properties", "x=123")
        chkFile(files, "blockchains/33/brid.txt", "F9161BC0ABE91C6ACAB68B4A0B18CCD5763AE5B5798BC522209DDCF6D0302630")
        chkFileBin(files, "blockchains/33/0.gtv", """{"signers":[]}""")
    }

    @Test fun testGtvMergeArray() {
        chkGtvMerge(
                """<array><int>1</int><int>2</int><int>3</int></array>""",
                """<array><int>4</int><int>5</int><int>6</int></array>""",
                """
                            <array>
                                <int>1</int>
                                <int>2</int>
                                <int>3</int>
                                <int>4</int>
                                <int>5</int>
                                <int>6</int>
                            </array>"""
        )

        chkGtvMerge(
                """<array><int>1</int><int>2</int><int>3</int></array>""",
                """<array merge="replace"><int>4</int><int>5</int><int>6</int></array>""",
                """
                            <array>
                                <int>4</int>
                                <int>5</int>
                                <int>6</int>
                            </array>"""
        )

        chkGtvMerge(
                """<array><int>1</int><int>2</int><int>3</int></array>""",
                """<array merge="append"><int>4</int><int>5</int><int>6</int></array>""",
                """
                            <array>
                                <int>1</int>
                                <int>2</int>
                                <int>3</int>
                                <int>4</int>
                                <int>5</int>
                                <int>6</int>
                            </array>"""
        )

        chkGtvMerge(
                """<array><int>1</int><int>2</int><int>3</int></array>""",
                """<array merge="prepend"><int>4</int><int>5</int><int>6</int></array>""",
                """
                            <array>
                                <int>4</int>
                                <int>5</int>
                                <int>6</int>
                                <int>1</int>
                                <int>2</int>
                                <int>3</int>
                            </array>"""
        )
    }

    @Test fun testGtvMergeDict() {
        chkGtvMerge(
                """<dict><entry key="A"><int>1</int></entry><entry key="B"><int>2</int></entry></dict>""",
                """<dict><entry key="C"><int>3</int></entry><entry key="D"><int>4</int></entry></dict>""",
                """
                            <dict>
                                <entry key="A">
                                    <int>1</int>
                                </entry>
                                <entry key="B">
                                    <int>2</int>
                                </entry>
                                <entry key="C">
                                    <int>3</int>
                                </entry>
                                <entry key="D">
                                    <int>4</int>
                                </entry>
                            </dict>"""
        )

        chkGtvMerge(
                """<dict><entry key="A"><int>1</int></entry><entry key="B"><int>2</int></entry></dict>""",
                """<dict><entry key="B"><int>3</int></entry><entry key="C"><int>4</int></entry></dict>""",
                """
                            <dict>
                                <entry key="A">
                                    <int>1</int>
                                </entry>
                                <entry key="B">
                                    <int>3</int>
                                </entry>
                                <entry key="C">
                                    <int>4</int>
                                </entry>
                            </dict>"""
        )

        chkGtvMerge(
                """<dict><entry key="A"><int>1</int></entry><entry key="B"><int>2</int></entry></dict>""",
                """<dict merge="replace"><entry key="B"><int>3</int></entry><entry key="C"><int>4</int></entry></dict>""",
                """
                            <dict>
                                <entry key="B">
                                    <int>3</int>
                                </entry>
                                <entry key="C">
                                    <int>4</int>
                                </entry>
                            </dict>"""
        )

        chkGtvMerge(
                """<dict><entry key="A"><int>1</int></entry><entry key="B"><int>2</int></entry></dict>""",
                """<dict merge="keep-old"><entry key="B"><int>3</int></entry><entry key="C"><int>4</int></entry></dict>""",
                """
                            <dict>
                                <entry key="A">
                                    <int>1</int>
                                </entry>
                                <entry key="B">
                                    <int>2</int>
                                </entry>
                                <entry key="C">
                                    <int>4</int>
                                </entry>
                            </dict>"""
        )

        chkGtvMerge(
                """<dict><entry key="A"><int>1</int></entry><entry key="B"><int>2</int></entry></dict>""",
                """<dict merge="keep-new"><entry key="B"><int>3</int></entry><entry key="C"><int>4</int></entry></dict>""",
                """
                            <dict>
                                <entry key="A">
                                    <int>1</int>
                                </entry>
                                <entry key="B">
                                    <int>3</int>
                                </entry>
                                <entry key="C">
                                    <int>4</int>
                                </entry>
                            </dict>"""
        )
    }

    @Test(expected = IllegalStateException::class)
    fun testGtvMergeDictStrict() {
        chkGtvMerge(
                """<dict><entry key="A"><int>1</int></entry><entry key="B"><int>2</int></entry></dict>""",
                """<dict merge="strict"><entry key="B"><int>3</int></entry><entry key="C"><int>4</int></entry></dict>""",
                ""
        )
    }

    @Test fun testGtvMergeDictEntry() {
        chkGtvMerge(
                """<dict><entry key="A"><int>1</int></entry><entry key="B"><int>2</int></entry></dict>""",
                """<dict><entry key="A"><int>3</int></entry><entry key="B"><int>4</int></entry></dict>""",
                """
                            <dict>
                                <entry key="A">
                                    <int>3</int>
                                </entry>
                                <entry key="B">
                                    <int>4</int>
                                </entry>
                            </dict>"""
        )

        chkGtvMerge(
                """<dict><entry key="A"><int>1</int></entry><entry key="B"><int>2</int></entry></dict>""",
                """<dict><entry key="A" merge="keep-old"><int>3</int></entry><entry key="B"><int>4</int></entry></dict>""",
                """
                            <dict>
                                <entry key="A">
                                    <int>1</int>
                                </entry>
                                <entry key="B">
                                    <int>4</int>
                                </entry>
                            </dict>"""
        )

        chkGtvMerge(
                """<dict><entry key="A"><int>1</int></entry><entry key="B"><int>2</int></entry></dict>""",
                """<dict><entry key="A" merge="keep-new"><int>3</int></entry><entry key="B"><int>4</int></entry></dict>""",
                """
                            <dict>
                                <entry key="A">
                                    <int>3</int>
                                </entry>
                                <entry key="B">
                                    <int>4</int>
                                </entry>
                            </dict>"""
        )

        chkGtvMerge(
                """<dict><entry key="A"><int>1</int></entry><entry key="B"><int>2</int></entry></dict>""",
                """<dict merge="keep-old"><entry key="A" merge="keep-new"><int>3</int></entry><entry key="B"><int>4</int></entry></dict>""",
                """
                            <dict>
                                <entry key="A">
                                    <int>3</int>
                                </entry>
                                <entry key="B">
                                    <int>2</int>
                                </entry>
                            </dict>"""
        )
    }

    @Test(expected = IllegalStateException::class)
    fun testGtvMergeDictEntryStrict() {
        chkGtvMerge(
                """<dict><entry key="A"><int>1</int></entry><entry key="B"><int>2</int></entry></dict>""",
                """<dict><entry key="A" merge="strict"><int>3</int></entry><entry key="B"><int>4</int></entry></dict>""",
                ""
        )
    }

    private fun chkGtvMerge(gtv1: String, gtv2: String, expected: String) {
        val files = generate(mapOf(), mapOf(), """
            <run>
                <nodes>
                    <config add-signers="false">x=y</config>
                </nodes>
                <chains>
                    <chain name="user" iid="33">
                        <config height="0">
                            <gtv path="foo/bar">$gtv1</gtv>
                            <gtv path="foo/bar">$gtv2</gtv>
                        </config>
                    </chain>
                </chains>
            </run>
        """)

        chkFile(files, "blockchains/33/0.xml", """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
            <dict>
                <entry key="foo">
                    <dict>
                        <entry key="bar">$expected
                        </entry>
                    </dict>
                </entry>
                <entry key="signers">
                    <array/>
                </entry>
            </dict>
        """)
    }

    @Test fun testGtvFromFile() {
        val configFiles = mapOf("part.xml" to """
            <dict>
                <entry key="a"><int>123</int></entry>
                <entry key="b"><int>456</int></entry>
            </dict>
        """)

        val files = generate(mapOf(), configFiles, """
            <run>
                <nodes>
                    <config add-signers="false">x=y</config>
                </nodes>
                <chains>
                    <chain name="user" iid="33">
                        <config height="0">
                            <gtv path="foo/bar" src="part.xml"/>
                        </config>
                    </chain>
                </chains>
            </run>
        """)

        chkFile(files, "blockchains/33/0.xml", """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
            <dict>
                <entry key="foo">
                    <dict>
                        <entry key="bar">
                            <dict>
                                <entry key="a">
                                    <int>123</int>
                                </entry>
                                <entry key="b">
                                    <int>456</int>
                                </entry>
                            </dict>
                        </entry>
                    </dict>
                </entry>
                <entry key="signers">
                    <array/>
                </entry>
            </dict>
        """)
    }

    private fun generate(
            sourceFiles: Map<String, String>,
            configFiles: Map<String, String>,
            confText: String
    ): MutableMap<String, DirFile> {
        val sourceDir = C_SourceDir.mapDirOf(sourceFiles)
        val configDir = MapGeneralDir(configFiles)
        val params = RellRunConfigParams(sourceDir, configDir, RellVersions.VERSION, unitTest = false)
        val conf = RellRunConfigGenerator.generate(TestRellCliEnv, params, "run.xml", confText.trimIndent())
        val files = RellRunConfigGenerator.buildFiles(conf).toMutableMap()
        return files
    }

    private fun chkFile(files: MutableMap<String, DirFile>, path: String, expected: String) {
        val actualFile = files.remove(path)
        assertNotNull(actualFile, "File not found: $path ${files.keys}")
        val actual = (actualFile as TextDirFile).text
        assertEquals(expected.trimIndent().trim(), actual.trim())
    }

    private fun chkFileBin(files: MutableMap<String, DirFile>, path: String, expected: String) {
        val actualFile = files.remove(path)
        assertNotNull(actualFile, "File not found: $path ${files.keys}")
        assertTrue(actualFile is BinaryDirFile, "File is not binary: $path")

        val bytes = actualFile.data.toByteArray()
        val gtv = GtvDecoder.decodeGtv(bytes)
        val actual = GtvTestUtils.encodeGtvStr(gtv)
        assertEquals(expected, actual)
    }
}
